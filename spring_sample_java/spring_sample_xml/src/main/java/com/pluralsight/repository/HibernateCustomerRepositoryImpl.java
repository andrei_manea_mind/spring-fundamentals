package com.pluralsight.repository;

import com.pluralsight.model.Customer;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.List;

public class HibernateCustomerRepositoryImpl implements CustomerRepository {

    @Value("${dbUsername}")
    private String dbUsername;

    @Override
    public List <Customer> findAll() {
        System.out.println(dbUsername);

        List<Customer> customerList = new ArrayList<>();

        Customer customer = new Customer();
        customer.setFirstname("Brian");
        customer.setLastname("Handsome");

        customerList.add(customer);
        return customerList;
    }
}
